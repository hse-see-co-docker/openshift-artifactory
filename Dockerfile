FROM docker.bintray.io/jfrog/artifactory-oss:latest

VOLUME /var/opt/jfrog/artifactory
VOLUME /var/opt/jfrog/artifactory/etc
VOLUME /data/artifactory

USER root

EXPOSE 8081

RUN chmod -R a+rw  /var/opt/jfrog && \
    chgrp -R 0  /var/opt/jfrog && \
    chmod -R g=u  /var/opt/jfrog && \
    chmod -R g=u /data/artifactory&& \
    chgrp -R 0 /data/artifactory && \
    chmod -R g=u /data/artifactory && \
    chmod -R g=u /opt/jfrog && \
    chgrp -R 0 /opt/jfrog && \
    chmod -R g=u /opt/jfrog


ENTRYPOINT ["/entrypoint-artifactory.sh"]

RUN useradd --badname 1017250000 -g 0 -g 1030
